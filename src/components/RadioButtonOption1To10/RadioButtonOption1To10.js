import * as React from 'react';
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";

import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";


export default function RadioButtonOption1To10({handleChange,name,value}) {
    return (
        <FormControl>
            <RadioGroup
                row
                aria-labelledby="demo-row-radio-buttons-group-label"
                name={name}
                value={value}
                onChange={handleChange}
            >
                <FormControlLabel value="1" control={<Radio  color="success"/>} label="1" />
                <FormControlLabel value="2" control={<Radio  color="success"/>} label="2" />
                <FormControlLabel value="3" control={<Radio  color="success"/>} label="3" />
                <FormControlLabel value="4" control={<Radio  color="success"/>} label="4" />
                <FormControlLabel value="5" control={<Radio  color="success"/>} label="5" />
                <FormControlLabel value="6" control={<Radio  color="success"/>} label="6" />
                <FormControlLabel value="7" control={<Radio  color="success"/>} label="7" />
                <FormControlLabel value="8" control={<Radio  color="success"/>} label="8" />
                <FormControlLabel value="9" control={<Radio  color="success"/>} label="9" />
                <FormControlLabel value="10" control={<Radio  color="success"/>} label="10" />
            </RadioGroup>
        </FormControl>
    );
}
