import * as React from 'react';
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";

import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";


export default function RowRadioButtonsGroup({handleChange,name,value}) {
    return (
        <FormControl>
            <RadioGroup
                row
                aria-labelledby="demo-row-radio-buttons-group-label"
                name={name}
                value={value}
                onChange={handleChange}
            >
                <FormControlLabel value="Not Relevent" control={<Radio color="success"/>} label="Not Relevent" />
                <FormControlLabel value="Yes" control={<Radio color="success" />} label="Yes" />
                <FormControlLabel value="No" control={<Radio  color="success"/>} label="No" />
            </RadioGroup>
        </FormControl>
    );
}
