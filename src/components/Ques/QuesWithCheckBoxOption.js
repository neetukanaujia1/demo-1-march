import React from 'react';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import RadicButtonOption from "../RadioButtonOption/RadicButtonOption";
import CheckBox from "../CheckBox";

function QuesWithCheckboxOption ({que,handleChange,value,name}) {

        return (

            <Grid container  spacing={1} direction={'column'}>
                <Grid item style={{textAlign: 'left'}}>
                    <Typography variant="h5">
                        {que}
                    </Typography>
                </Grid>
                <Grid item style={{textAlign: 'left'}}>
                    <CheckBox handleChange={handleChange} name={name}
                                       value={value}/>
                </Grid>
            </Grid>

        );

}

export default QuesWithCheckboxOption;
