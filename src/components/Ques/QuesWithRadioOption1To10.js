import React from 'react';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import RadicButtonOption1To10 from "../RadioButtonOption1To10";
function QuesWithRadioOption1To10 ({que,handleChange,value,name}) {
        return (
            <Grid container justify={'space-start'} spacing={1} direction={'column'}>
                <Grid item style={{textAlign: 'left'}}><Typography variant="h5">{que}</Typography></Grid>
                <Grid item style={{textAlign: 'left'}}><RadicButtonOption1To10 handleChange={handleChange} name={name} value={value}/>
                </Grid>
            </Grid>
        );
}
export default QuesWithRadioOption1To10;
