import React from 'react';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import RadicButtonOption1To10 from "../RadioButtonOption1To10";
import CheckBox from "../CheckBox";
function QuesWithCheckboxOption ({que,handleChange,value,name}) {
        return (
            <Grid container justify={'space-start'} spacing={1} direction={'column'}
                  alignItems={'center'}>
                <Grid item><Typography variant="h5">{que}</Typography></Grid>
                <Grid item><CheckBox handleChange={handleChange} name={name} value={value}/>
                </Grid>
            </Grid>
        );
}
export default QuesWithCheckboxOption;
