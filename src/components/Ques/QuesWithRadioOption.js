import React from 'react';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import RadicButtonOption from "../RadioButtonOption/RadicButtonOption";

function QuesWithRadioOption ({que,handleChange,value,name}) {

        return (

            <Grid container justify={'space-between'} spacing={1} direction={'row'}
                  alignItems={'center'}>
                <Grid item>
                    <Typography variant="h5">
                        {que}
                    </Typography>
                </Grid>
                <Grid item>
                    <RadicButtonOption handleChange={handleChange} name={name}
                                       value={value}/>
                </Grid>
            </Grid>

        );

}

export default QuesWithRadioOption;
