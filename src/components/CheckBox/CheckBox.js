import * as React from 'react';
import Grid from "@material-ui/core/Grid";
import {useEffect, useState} from "react";
import Typography from "@material-ui/core/Typography";


export default function CheckBox({handleChange,name,value}) {
    const [checkedAll, setCheckedAll] = useState(false);
    const [checked, setChecked] = useState({
        not_relevent: false,
        when_lying_down: false,
        when_sitting: false,
        under_standing: false,
        in_walk: false
    });


    const toggleCheck = (inputName) => {
        setChecked((prevState) => {
            const newState = { ...prevState };
            newState[inputName] = !prevState[inputName];
            return newState;
        });
    };



    const selectAll = (value) => {
        setCheckedAll(value);
        setChecked((prevState) => {
            const newState = { ...prevState };
            for (const inputName in newState) {
                newState[inputName] = value;
            }
            return newState;
        });
    };



    useEffect(() => {
        let allChecked = true;
        for (const inputName in checked) {
            if (checked[inputName] === false) {
                allChecked = false;
            }
        }
        if (allChecked) {
            setCheckedAll(true);
        } else {
            setCheckedAll(false);
        }
        console.log(checked)
    }, [checked]);
    return (

       <Grid container justify={'space-between'} spacing={1} direction={'row'}
             alignItems={'center'}>
           <div className="App">
               <Grid container justify={'space-start'} spacing={1} direction={'row'}>
                   <Grid item>
                       <input
                           type="checkbox"
                           onChange={(event) => selectAll(event.target.checked)}
                           checked={checkedAll}
                       /></Grid>
                   <Grid item>
                       <label> <Typography variant="body1">All</Typography></label>
                   </Grid>
               </Grid>
               <Grid container justify={'space-start'} spacing={1} direction={'row'}>
                   <Grid item>

                   <input
                       type="checkbox"
                       name="not_relevent"
                       onChange={() => toggleCheck("not_relevent")}
                       checked={checked["not_relevent"]}
                   />
                   </Grid>
                   <Grid item>
                   <label> <Typography variant="body1">Not Relevent</Typography></label>
                   </Grid>
               </Grid>
               <Grid container justify={'space-start'} spacing={1} direction={'row'}>
                   <Grid item>

                   <input
                       type="checkbox"
                       name="when_lying_down"
                       onChange={() => toggleCheck("when_lying_down")}
                       checked={checked["when_lying_down"]}
                   />
                   </Grid>
                   <Grid item>
                   <label> <Typography variant="body1">When Lying Down</Typography></label>
                   </Grid>
               </Grid>  
               
               <Grid container justify={'space-start'} spacing={1} direction={'row'}>
                   <Grid item>

                   <input
                       type="checkbox"
                       name="when_sitting"
                       onChange={() => toggleCheck("when_sitting")}
                       checked={checked["when_sitting"]}
                   />
                   </Grid>
                   <Grid item>
                   <label> <Typography variant="body1">When Sitting</Typography></label>
                   </Grid>
               </Grid>
               <Grid container justify={'space-start'} spacing={1} direction={'row'}>
                   <Grid item>

                   <input
                       type="checkbox"
                       name="under_standing"
                       onChange={() => toggleCheck("under_standing")}
                       checked={checked["under_standing"]}
                   />
                   </Grid>
                   <Grid item>
                   <label> <Typography variant="body1">Under Standing</Typography></label>
                   </Grid>
               </Grid>
               <Grid container justify={'space-start'} spacing={1} direction={'row'}>
                   <Grid item>

                   <input
                       type="checkbox"
                       name="in_walk"
                       onChange={() => toggleCheck("in_walk")}
                       checked={checked["in_walk"]}
                   />
                   </Grid>
                   <Grid item>
                   <label> <Typography variant="body1">In Walk</Typography></label>
                   </Grid>
               </Grid>

           </div>
       </Grid>
    );
}
