import {applyMiddleware, combineReducers, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import notification from './notification/reducer';
const initialState = {};
const middlewares = [
    thunkMiddleware,
];
const rootReducer = combineReducers({
    notification,
});
const store = createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(...middlewares)));
export default store;
