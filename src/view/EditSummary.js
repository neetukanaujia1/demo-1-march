import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from "@material-ui/core/IconButton";
import EditIcon from '@material-ui/icons/Edit';
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";

export default function AlertDialog({setFormState,handleEditSummmary, handleChangeField, formState,setId,currentId}) {
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
        setId(currentId);
        setFormState(formState => ({
            ...formState,
            values: {},
        }));
    };
    const handleClose = () => {
        setOpen(false);
        setFormState(formState => ({
            ...formState,
            values: {},
        }));
    };
    return (
        <div>
            <IconButton onClick={handleClickOpen} aria-label="delete">
                <EditIcon/>
            </IconButton>
            <Dialog
                open={open}
                fullWidth={true}
                maxWidth={'lg'}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Edit Summary"}</DialogTitle>
                <DialogContent>
                    <Grid>
                        <Grid item>
                            <TextField autoComplete="off"
                                       fullWidth={'true'}
                                       placeholder="Topic"
                                       name="name"
                                       type="text"
                                       onChange={handleChangeField}
                                       value={formState.values.name}
                                       variants='outlined'
                            />
                            <TextField autoComplete="off"
                                       fullWidth={'true'}
                                       placeholder="Description"
                                       name="description"
                                       type="text"
                                       multiline
                                       maxRows={8}
                                       rows={8}
                                       variants='outlined'
                                       onChange={handleChangeField}
                                       value={formState.values.description}
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Close
                    </Button>
                    <Button onClick={handleEditSummmary} color="primary" autoFocus>
                        Edit
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
