import React, {useEffect, useState} from 'react';
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import QuesWithRadioOption from "../components/Ques/QuesWithRadioOption";
import QuesWithRadioOption1To10 from "../components/Ques/QuesWithRadioOption1To10";
import Button from "../components/Button";
import {Link} from "react-router-dom";
import QuesWithCheckboxOption from "../components/Ques/QuesWithCheckBoxOption";
function Home() {
    const title = 'Pain and Fuctional Description';
    const [formState, setFormState] = useState({
        values: {},
    });
    const list = [{
        name: 'ans1',
        description: 'have you been diagnosed with this problem ?',
        type: 'radio',
        value: formState.values.ans1
    }, {
        name: 'ans2',
        description: 'test2',
        type: 'radio',
        value: formState.values.ans2
    },

        {
        name: 'ans3',
        description: 'When do you experience problem ?',
        type: 'checkbox',
        value: formState.values.ans3
    },



        {
        name: 'ans6',
        description: 'How intense is experience of your problem rate on the scale of 1 to 10 ',
        type: 'radio1to10',
        value: formState.values.ans6
    },
    ]
    useEffect(() => {
        setFormState(formState => ({
            ...formState,
        }));
        console.log("---", formState.values);
    }, [formState.values]);
    const handleChange = event => {
        event.persist();
        setFormState(formState => ({
            ...formState,
            values: {
                ...formState.values,
                [event.target.name]:
                    event.target.type === 'checkbox'
                        ? event.target.checked
                        : event.target.value
            },
        }));
    };
    return (
        <Grid>
            <Grid align={'center'}>
                <Container maxWidth="lg">
                    <Paper style={{
                        padding: 30,
                        margin: '20px auto',
                    }} elevation={10}>
                        <Grid container justify={'space-between'} spacing={1} direction={'column'}
                              alignItems={'center'}>
                            <Grid item>
                                <Typography color="primary" variant="h3">
                                    {title}
                                </Typography>
                            </Grid>


                            {
                                list.map(data => {
                                    return (
                                        data.type === 'radio' ?

                                                <QuesWithRadioOption name={data.name} que={data.description}
                                                                     handleChange={handleChange} value={data.value}/>

                                            : data.type === 'checkbox' ?

                                            <QuesWithCheckboxOption name={data.name} que={data.description}
                                                                      handleChange={handleChange} value={data.value}/>

                                            : <QuesWithRadioOption1To10 name={data.name} que={data.description}
                                                                        handleChange={handleChange} value={data.value}/>

                                    );
                                })
                            }


                        </Grid>
                        <hr/>
                        <Grid container justify={'center'} spacing={1} direction={'row'} alignItems={'center'}>
                            <Grid item>
                                <Button
                                    style={{textDecoration: 'none'}}
                                >
                                    <Link to={'/summary'}>
                                     Next
                                    </Link>
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Container>
            </Grid>
        </Grid>
    );
}

export default Home;
