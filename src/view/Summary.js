import React, {useEffect, useState} from 'react';
import {withStyles, makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {mainColor} from "../config";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import EditSummary from "./EditSummary";
import {addNotification} from "../store/notification/actions";
import {useDispatch} from "react-redux";
import Button from "../components/Button";
import {Link} from "react-router-dom";
const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: mainColor,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);
const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);
const useStyles = makeStyles({
    table: {
        minWidth: 700,
    },
});
export default function CustomizedTables() {
    const dispatch = useDispatch();
    const classes = useStyles();
    const title = 'Summary';
    let list = [
        {
            id: 1,
            name: 'Dignosis',
            description: 'have you been diagnosed with this problem ?',
        },
        {
            id: 2,
            name: 'Dignosis',
            description: 'have you been diagnosed with this problem ?',
        },
        {
            id: 3,
            name: 'Dignosis',
            description: 'have you been diagnosed with this problem ?',
        },
    ]
    const [data, setData] = useState(list);
    const [id, setId] = useState(null);
    const [formState, setFormState] = useState({
        values: {
            name: '',
            description: ''
        },
    });
    useEffect(()=>{
    },[data]);
    const handleEditSummmary = (event) => {
        event.persist();
        console.log('id: ' + id);
        console.log(         formState.values.name, formState.values.description);
        let newArr = data.map((item, i) => {
            if (id === item.id) {
                return { ...item, name: formState.values.name, description: formState.values.description, };
            } else {
                return item;
            }
        });
        setData(newArr);
        console.log('data ',data);
        dispatch(addNotification({
            message: 'Edit !!!',
            type: 'success',
        }));
    }
    const handleChangeField = (event) => {
        event.persist();
        setFormState(formState => ({
            ...formState,
            values: {
                ...formState.values,
                [event.target.name]: event.target.value
            },
        }));
    }
    useEffect(() => {
        setFormState(formState => ({
            ...formState,
        }));
    }, [formState.values]);
    return (
        <Grid>
            <Grid align={'center'}>
                <Container maxWidth="lg">
                    <Paper style={{
                        padding: 30,
                        margin: '20px auto',
                    }} elevation={10}>
                        <Grid container justify={'space-start'} spacing={1} direction={'column'}
                              alignItems={'left'}>
                            <Grid item>
                                <Typography color="primary" variant="h3">
                                    {title}
                                </Typography>
                            </Grid>
                            <TableContainer>
                                <Table className={classes.table} aria-label="customized table">
                                    <TableHead>
                                        <TableRow>
                                            <StyledTableCell align="center">#</StyledTableCell>
                                            <StyledTableCell align="center">Topic</StyledTableCell>
                                            <StyledTableCell align="center">Description</StyledTableCell>
                                            <StyledTableCell align="center">Action</StyledTableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {data.map((row, index) => (
                                            <StyledTableRow key={index}>
                                                <StyledTableCell align="center">{index + 1}</StyledTableCell>
                                                <StyledTableCell align="center">
                                                    {row.name}
                                                </StyledTableCell>
                                                <StyledTableCell align="center">{row.description}</StyledTableCell>
                                                <StyledTableCell align="center"><EditSummary
                                                    handleChangeField={handleChangeField}
                                                    formState={formState}
                                                    setFormState={setFormState}
                                                    handleEditSummmary={handleEditSummmary} setId={setId}
                                                    currentId={row.id}/></StyledTableCell>
                                            </StyledTableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                        <hr/>
                        <Grid container justify={'center'} spacing={1} direction={'row'} alignItems={'center'}>
                            <Grid item>
                                <Button
                                    style={{textDecoration: 'none'}}
                                >
                                    <Link to={'/'}>
                                        Back
                                    </Link>
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Container>
            </Grid>
        </Grid>
    );
}
