import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import SnackBar from "./components/SnackBar";
import {removeLastNotification} from "./store/notification/actions";
import ROUTES, {RenderRoutes} from "./routes";
function Main() {
    const dispatch = useDispatch();
    const notification = useSelector(state => state.notification);
    return (
        <div>
            {notification.current &&
            <SnackBar
                onClose={() => (dispatch(removeLastNotification()))}
                variant={notification.current.type}
                autoHideDuration={notification.current.duration || 3000}
                message={notification.current.message || "Something went wrong"}
            />
            }
            <RenderRoutes routes={ROUTES}/>
        </div>
    );
}
export default Main;
