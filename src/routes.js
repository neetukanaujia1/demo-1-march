import React from "react";
import {Route, Switch} from "react-router-dom";
import Home from "./view/Home";
import Summary from "./view/Summary";

const ROUTES = [
    {
        path: "/",
        exact: true,
        component: Home
    },
    {
        path: "/summary",
        exact: true,
        component: Summary
    },
    {
        component: () => <h1>Error 404</h1>,
    },
];
function RouteWithSubRoutes(route) {
    return (
        <Route
            path={route.path}
            exact={route.exact}
            render={props => <route.component {...props} routes={route.routes}/>}
        />
    );
}
export function RenderRoutes({routes}) {
    return (
        <Switch>
            {routes.map((route, i) => {
                return <RouteWithSubRoutes key={route.key} {...route} />;
            })}
            <Route component={() => <h1>Not Found!</h1>}/>
        </Switch>
    );
}
export default ROUTES;
